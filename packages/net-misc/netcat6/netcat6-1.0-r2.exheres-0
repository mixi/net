# Copyright 2009 A Frederick Christensen <fauxmight@nosocomia.com>
# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A simple utility which reads and writes data across network connections"
DESCRIPTION="
Netcat6 is a total rewrite of netcat, with several advantages:
    * It fully supports IPv6.
    * It is far more efficient, utilizing flexible buffering and minimal (or no)
      data copying or analysis.
    * The source is well structured, documented and very easy to follow. One of
      the main objectives of netcat6 is to produce an excellent example of AF
      independant networking and efficient data transfer. The code has minimal
      dependancy on the address family or protocol type and can be trivially
      extended to talk many layer 3 protocols.
    * Greatly improved configuration and platform indipendence.
    * Can support servers or clients that use TCP half-close.
"

MY_PNV="nc6-${PV}"

HOMEPAGE="http://www.deepspace6.net/projects/netcat6.html"
DOWNLOADS="http://ftp.deepspace6.net/pub/ds6/sources/nc6/${MY_PNV}.tar.bz2"
REMOTE_IDS="sourceforge:netcat6"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="bluetooth"

DEPENDENCIES="
    build+run:
        bluetooth? ( net-wireless/bluez )
    run:
        !net-misc/netcat [[ description = [ Both install /usr/bin/nc and man pages ]
                            resolution = manual ]]
"

BUGS_TO="fauxmight@nosocomia.com spoonb@exherbo.org"

WORK="${WORKBASE}/${MY_PNV}/"

DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-nls --enable-ipv6 --hates={doc,dataroot}dir )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( "bluetooth bluez" )
DEFAULT_SRC_COMPILE_PARAMS=( AR="${AR}" )

src_install() {
    default
    dosym nc6 /usr/$(exhost --target)/bin/nc
    dosym nc6.1 /usr/share/man/man1/nc.1
}

